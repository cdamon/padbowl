package com.jhipster.padbowl.kipe;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ActionsUnitTest {

  @Test
  void shouldNotBuildWithDuplicatedActions() {
    assertThatThrownBy(() -> new Actions(Action.all("action", Resource.GAMES), Action.all("action", Resource.GAMES)))
      .isExactlyInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void shouldNotBeAuthorizedWithoutAction() {
    assertThat(new Actions((Action[]) null).allAuthorized("action", Resource.GAMES)).isFalse();
  }

  @Test
  void shouldCheckAuthorizations() {
    Actions actions = new Actions(Action.all("action", Resource.GAMES), Action.specific("action", Resource.PLAYERS));

    assertThat(actions.allAuthorized("dummy", Resource.GAMES)).isFalse();
    assertThat(actions.allAuthorized("action", Resource.GAMES)).isTrue();
    assertThat(actions.allAuthorized("action", Resource.PLAYERS)).isFalse();
    assertThat(actions.specificAuthorized("dummy", Resource.GAMES)).isFalse();
    assertThat(actions.specificAuthorized("action", Resource.GAMES)).isTrue();
    assertThat(actions.specificAuthorized("action", Resource.PLAYERS)).isTrue();
  }
}
