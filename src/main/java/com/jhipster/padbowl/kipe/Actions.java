package com.jhipster.padbowl.kipe;

import java.util.Set;

class Actions {
  private final Set<Action> actions;

  public Actions(Action... actions) {
    this.actions = buildActions(actions);
  }

  private Set<Action> buildActions(Action... actions) {
    if (actions == null) {
      return Set.of();
    }

    return Set.of(actions);
  }

  public boolean allAuthorized(String action, Resource resource) {
    return actions.contains(Action.all(action, resource));
  }

  public boolean specificAuthorized(String action, Resource resource) {
    return actions.contains(Action.specific(action, resource)) || allAuthorized(action, resource);
  }
}
