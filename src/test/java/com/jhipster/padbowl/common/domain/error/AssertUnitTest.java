package com.jhipster.padbowl.common.domain.error;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AssertUnitTest {

  @Test
  void shouldNotValidateNullInputsAsNotNull() {
    assertThatThrownBy(() -> Assert.notNull("field", null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("\"field\"");
  }

  @Test
  void shouldNotValidateNullStringAsNotBlank() {
    assertThatThrownBy(() -> Assert.notBlank("field", null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("\"field\"")
      .hasMessageContaining("(null)");
  }

  @Test
  void shouldNotValidateEmptyStringAsNotBlank() {
    assertNotBlankString("");
  }

  @Test
  void shouldNotValidateSpaceStringAsNotBlank() {
    assertNotBlankString(" ");
  }

  @Test
  void shouldNotValidateTabStringAsNotBlank() {
    assertNotBlankString("  ");
  }

  private void assertNotBlankString(String input) {
    assertThatThrownBy(() -> Assert.notBlank("field", input))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("\"field\"")
      .hasMessageContaining("(blank)");
  }

  @Test
  void shouldValidateStringWithValueAsNotBlank() {
    assertThatCode(() -> Assert.notBlank("field", "value")).doesNotThrowAnyException();
  }

  @Test
  void shouldNotValidateBlankStringWhenValidatingField() {
    assertThatThrownBy(() -> Assert.field("field", null).notBlank())
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("null");
  }

  @Test
  void shouldValidateMaxLengthForNullString() {
    assertThatCode(() -> Assert.field("field", null).maxLength(5)).doesNotThrowAnyException();
  }

  @Test
  void shouldValidateShortEnoughString() {
    assertThatCode(() -> Assert.field("field", "value").maxLength(50)).doesNotThrowAnyException();
    assertThatCode(() -> Assert.field("field", "value").maxLength(5)).doesNotThrowAnyException();
  }

  @Test
  void shouldNotValidateTooLongString() {
    assertThatThrownBy(() -> Assert.field("field", "value").maxLength(4))
      .isExactlyInstanceOf(StringTooLongException.class)
      .hasMessageContaining("\"field\"")
      .hasMessageContaining("5")
      .hasMessageContaining("4");
  }
}
