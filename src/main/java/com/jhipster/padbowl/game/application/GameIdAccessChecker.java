package com.jhipster.padbowl.game.application;

import com.jhipster.padbowl.game.domain.Game;
import com.jhipster.padbowl.game.domain.GameId;
import com.jhipster.padbowl.kipe.CanChecker;
import com.jhipster.padbowl.kipe.PadBowlAuthorizations;
import com.jhipster.padbowl.kipe.Resource;
import java.util.function.Function;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
class GameIdAccessChecker implements CanChecker<GameId> {
  private final GamesApplicationService games;

  public GameIdAccessChecker(GamesApplicationService games) {
    this.games = games;
  }

  @Override
  public boolean can(Authentication authentication, String action, GameId item) {
    if (PadBowlAuthorizations.specificAuthorized(authentication, action, Resource.GAMES)) {
      return games.get(item).map(isPlayerGame(authentication)).orElse(false);
    }

    return false;
  }

  private Function<Game, Boolean> isPlayerGame(Authentication authentication) {
    return game -> game.getPlayer().get().equals(PadBowlAuthorizations.getUsername(authentication).get());
  }
}
