package com.jhipster.padbowl.hall;

import com.jhipster.padbowl.common.domain.Guttered;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.PayloadApplicationEvent;
import org.springframework.stereotype.Component;

@Component
public class Shamer implements ApplicationListener<PayloadApplicationEvent<Guttered>> {
  private static final Logger log = LoggerFactory.getLogger(Shamer.class);

  private final String destination;

  public Shamer(@Value("${shame.destination}") String destination) {
    this.destination = destination;
  }

  @Override
  public void onApplicationEvent(PayloadApplicationEvent<Guttered> event) {
    try {
      Files.write(Paths.get(destination), event.getPayload().getPlayerName().get().getBytes(), StandardOpenOption.CREATE_NEW);
    } catch (IOException e) {
      log.error("Can't shame on {}: {}", destination, e.getMessage(), e);
    }
  }
}
