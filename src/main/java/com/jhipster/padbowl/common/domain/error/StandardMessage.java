package com.jhipster.padbowl.common.domain.error;

public enum StandardMessage implements PadBowlMessage {
  USER_MANDATORY("user.mandatory"),
  BAD_REQUEST("user.bad-request"),
  INTERNAL_SERVER_ERROR("server.internal-server-error"),
  SERVER_MANDATORY_NULL("server.mandatory-null"),
  SERVER_MANDATORY_BLANK("server.mandatory-blank"),
  STRING_TOO_LONG("server.string-too-long");

  private final String messageKey;

  private StandardMessage(String code) {
    this.messageKey = code;
  }

  @Override
  public String getMessageKey() {
    return messageKey;
  }
}
