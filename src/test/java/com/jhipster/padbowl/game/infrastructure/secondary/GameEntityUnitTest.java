package com.jhipster.padbowl.game.infrastructure.secondary;

import static com.jhipster.padbowl.game.domain.GamesFixture.*;
import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class GameEntityUnitTest {

  @Test
  void shouldConvertFromAndToGame() {
    assertThat(GameEntity.from(threeRolls()).toDomain()).usingRecursiveComparison().isEqualTo(threeRolls());
  }
}
