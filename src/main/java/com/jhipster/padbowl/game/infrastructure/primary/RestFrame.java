package com.jhipster.padbowl.game.infrastructure.primary;

import com.jhipster.padbowl.game.domain.Frame;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Frame", description = "Frame in a game")
class RestFrame {
  @ApiModelProperty(value = "Pins down in the first roll", required = true)
  private final int firstRoll;

  @ApiModelProperty("Pins down in the second roll")
  private final Integer secondRoll;

  private RestFrame(int firstRoll, Integer secondRoll) {
    this.firstRoll = firstRoll;
    this.secondRoll = secondRoll;
  }

  static RestFrame from(Frame frame) {
    return new RestFrame(frame.getFirstRoll(), frame.getSecondRoll().orElse(null));
  }

  public int getFirstRoll() {
    return firstRoll;
  }

  public Integer getSecondRoll() {
    return secondRoll;
  }
}
