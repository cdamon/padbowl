/**
 *
 */
package com.jhipster.padbowl.common.infrastructure.primary.error;

import com.jhipster.padbowl.common.domain.error.ErrorStatus;
import com.jhipster.padbowl.common.domain.error.PadBowlException;
import com.jhipster.padbowl.common.domain.error.StandardMessage;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class PadBowlErrorHandler extends ResponseEntityExceptionHandler {
  private static final String MESSAGE_PREFIX = "padbowl.error.";
  private static final String DEFAULT_KEY = StandardMessage.INTERNAL_SERVER_ERROR.getMessageKey();
  private static final String BAD_REQUEST_KEY = StandardMessage.BAD_REQUEST.getMessageKey();
  private static final String STATUS_EXCEPTION_KEY = "status-exception";

  private static final Logger logger = LoggerFactory.getLogger(PadBowlErrorHandler.class);

  private final MessageSource messages;

  public PadBowlErrorHandler(MessageSource messages) {
    Locale.setDefault(Locale.ENGLISH);
    this.messages = messages;
  }

  @ExceptionHandler
  public ResponseEntity<PadBowlError> handlePadBowlException(PadBowlException exception) {
    HttpStatus status = getStatus(exception);

    logError(exception, status);

    String messageKey = getMessageKey(status, exception);
    PadBowlError error = new PadBowlError(messageKey, getMessage(messageKey, exception.getArguments()), null);
    return new ResponseEntity<>(error, status);
  }

  @ExceptionHandler
  public ResponseEntity<PadBowlError> handleResponseStatusException(ResponseStatusException exception) {
    HttpStatus status = exception.getStatus();

    logError(exception, status);

    PadBowlError error = new PadBowlError(STATUS_EXCEPTION_KEY, buildErrorStatusMessage(exception), null);
    return new ResponseEntity<>(error, status);
  }

  private String buildErrorStatusMessage(ResponseStatusException exception) {
    String reason = exception.getReason();

    if (StringUtils.isBlank(reason)) {
      Map<String, String> statusArgument = Map.of("status", String.valueOf(exception.getStatus().value()));

      return getMessage(STATUS_EXCEPTION_KEY, statusArgument);
    }

    return reason;
  }

  @ExceptionHandler(MaxUploadSizeExceededException.class)
  public ResponseEntity<PadBowlError> handleFileSizeException(MaxUploadSizeExceededException maxUploadSizeExceededException) {
    logger.warn("File size limit exceeded: {}", maxUploadSizeExceededException.getMessage(), maxUploadSizeExceededException);

    PadBowlError error = new PadBowlError("server.upload-too-big", getMessage("server.upload-too-big", null), null);
    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(AccessDeniedException.class)
  public ResponseEntity<PadBowlError> handleAccessDeniedException(AccessDeniedException accessDeniedException) {
    PadBowlError error = new PadBowlError("user.access-denied", getMessage("user.access-denied", null), null);
    return new ResponseEntity<>(error, HttpStatus.FORBIDDEN);
  }

  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  public ResponseEntity<PadBowlError> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException exception) {
    Throwable rootCause = ExceptionUtils.getRootCause(exception);
    if (rootCause instanceof PadBowlException) {
      return handlePadBowlException((PadBowlException) rootCause);
    }

    PadBowlError error = new PadBowlError(BAD_REQUEST_KEY, getMessage(BAD_REQUEST_KEY, null), null);
    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }

  private HttpStatus getStatus(PadBowlException exception) {
    ErrorStatus status = exception.getStatus();
    if (status == null) {
      return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    switch (status) {
      case BAD_REQUEST:
        return HttpStatus.BAD_REQUEST;
      case UNAUTHORIZED:
        return HttpStatus.UNAUTHORIZED;
      case FORBIDDEN:
        return HttpStatus.FORBIDDEN;
      case NOT_FOUND:
        return HttpStatus.NOT_FOUND;
      case CONFLICT:
        return HttpStatus.CONFLICT;
      default:
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }
  }

  private void logError(Exception exception, HttpStatus status) {
    if (status.is5xxServerError()) {
      logger.error("A server error was sent to a user: {}", exception.getMessage(), exception);

      logErrorBody(exception);
    } else {
      logger.warn("An error was sent to a user: {}", exception.getMessage(), exception);
    }
  }

  private void logErrorBody(Exception exception) {
    Throwable cause = exception.getCause();
    if (cause instanceof RestClientResponseException) {
      RestClientResponseException restCause = (RestClientResponseException) cause;

      logger.error("Cause body: {}", restCause.getResponseBodyAsString());
    }
  }

  private String getMessageKey(HttpStatus status, PadBowlException exception) {
    if (exception.getPadBowlMessage() == null) {
      return getDefaultMessage(status);
    }

    return exception.getPadBowlMessage().getMessageKey();
  }

  private String getDefaultMessage(HttpStatus status) {
    if (status.is5xxServerError()) {
      return DEFAULT_KEY;
    }

    return BAD_REQUEST_KEY;
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
    MethodArgumentNotValidException exception,
    HttpHeaders headers,
    HttpStatus status,
    WebRequest request
  ) {
    logger.debug("Bean validation error {}", exception.getMessage(), exception);

    PadBowlError error = new PadBowlError(BAD_REQUEST_KEY, getMessage(BAD_REQUEST_KEY, null), getFieldsError(exception));
    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler
  public ResponseEntity<PadBowlError> handleBeanValidationError(ConstraintViolationException exception) {
    logger.debug("Bean validation error {}", exception.getMessage(), exception);

    PadBowlError error = new PadBowlError(BAD_REQUEST_KEY, getMessage(BAD_REQUEST_KEY, null), getFieldsErrors(exception));
    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }

  @Override
  protected ResponseEntity<Object> handleBindException(
    BindException exception,
    HttpHeaders headers,
    HttpStatus status,
    WebRequest request
  ) {
    List<PadBowlFieldError> fieldErrors = exception.getFieldErrors().stream().map(toPadBowlFieldError()).collect(Collectors.toList());

    PadBowlError error = new PadBowlError(BAD_REQUEST_KEY, getMessage(BAD_REQUEST_KEY, null), fieldErrors);

    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }

  private ConstraintViolation<?> extractSource(FieldError error) {
    return error.unwrap(ConstraintViolation.class);
  }

  private String getMessage(String messageKey, Map<String, String> arguments) {
    String text = getMessageFromSource(messageKey);

    return ArgumentsReplacer.replaceArguments(text, arguments);
  }

  private String getMessageFromSource(String messageKey) {
    Locale locale = LocaleContextHolder.getLocale();

    try {
      return messages.getMessage(MESSAGE_PREFIX + messageKey, null, locale);
    } catch (NoSuchMessageException e) {
      return messages.getMessage(MESSAGE_PREFIX + DEFAULT_KEY, null, locale);
    }
  }

  private List<PadBowlFieldError> getFieldsErrors(ConstraintViolationException exception) {
    return exception.getConstraintViolations().stream().map(toFieldError()).collect(Collectors.toList());
  }

  private List<PadBowlFieldError> getFieldsError(MethodArgumentNotValidException exception) {
    return exception.getBindingResult().getFieldErrors().stream().map(toPadBowlFieldError()).collect(Collectors.toList());
  }

  private Function<FieldError, PadBowlFieldError> toPadBowlFieldError() {
    return error ->
      PadBowlFieldError
        .builder()
        .fieldPath(error.getField())
        .reason(error.getDefaultMessage())
        .message(getMessage(error.getDefaultMessage(), buildArguments(extractSource(error))))
        .build();
  }

  private Map<String, String> buildArguments(ConstraintViolation<?> violation) {
    return violation
      .getConstraintDescriptor()
      .getAttributes()
      .entrySet()
      .stream()
      .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().toString()));
  }

  private Function<ConstraintViolation<?>, PadBowlFieldError> toFieldError() {
    return violation -> {
      Map<String, String> arguments = buildArguments(violation);

      String message = violation.getMessage();
      return PadBowlFieldError
        .builder()
        .fieldPath(violation.getPropertyPath().toString())
        .reason(message)
        .message(getMessage(message, arguments))
        .build();
    };
  }

  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(
    HttpMessageNotReadableException exception,
    HttpHeaders headers,
    HttpStatus status,
    WebRequest request
  ) {
    logger.error("Error reading query information: {}", exception.getMessage(), exception);

    PadBowlError error = new PadBowlError(DEFAULT_KEY, getMessage(DEFAULT_KEY, null), null);
    return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler
  public ResponseEntity<PadBowlError> handleAuthenticationException(AuthenticationException exception) {
    logger.debug("A user tried to do an unauthorized operation: {}", exception.getMessage(), exception);

    String message = AuthenticationMessage.NOT_AUTHENTICATED.getMessageKey();
    PadBowlError error = new PadBowlError(message, getMessage(message, null), null);

    return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
  }

  @ExceptionHandler
  public ResponseEntity<PadBowlError> handleRuntimeException(Throwable throwable) {
    logger.error("An unhandled error occurs: {}", throwable.getMessage(), throwable);

    PadBowlError error = new PadBowlError(DEFAULT_KEY, getMessage(DEFAULT_KEY, null), null);
    return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
