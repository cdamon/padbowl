package com.jhipster.padbowl.game.domain;

import com.jhipster.padbowl.common.domain.error.Assert;
import net.logstash.logback.encoder.org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;

public class PlayerName {
  private final String name;

  public PlayerName(String name) {
    Assert.field("playerName", name).notBlank().maxLength(50);

    this.name = name;
  }

  public String get() {
    return name;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(name).hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }

    PlayerName other = (PlayerName) obj;
    return new EqualsBuilder().append(name, other.name).isEquals();
  }
}
