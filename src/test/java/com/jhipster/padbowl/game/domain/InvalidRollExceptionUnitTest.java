package com.jhipster.padbowl.game.domain;

import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.common.domain.error.ErrorStatus;
import org.junit.jupiter.api.Test;

class InvalidRollExceptionUnitTest {

  @Test
  void shouldGetExceptionInformation() {
    InvalidRollException exception = new InvalidRollException();

    assertThat(exception.getPadBowlMessage()).isEqualTo(GameMessage.INVALID_ROLL);
    assertThat(exception.getMessage()).contains("invalid roll");
    assertThat(exception.getStatus()).isEqualTo(ErrorStatus.BAD_REQUEST);
  }
}
