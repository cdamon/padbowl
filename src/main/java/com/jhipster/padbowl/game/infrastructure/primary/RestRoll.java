package com.jhipster.padbowl.game.infrastructure.primary;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "roll", description = "Roll in game")
class RestRoll {
  @ApiModelProperty(value = "Number of pins down", required = true)
  private final int roll;

  public RestRoll(@JsonProperty("roll") int roll) {
    this.roll = roll;
  }

  public int getRoll() {
    return roll;
  }
}
