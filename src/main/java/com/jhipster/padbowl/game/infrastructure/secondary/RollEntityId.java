package com.jhipster.padbowl.game.infrastructure.secondary;

import com.jhipster.padbowl.common.infrastructure.Generated;
import java.io.Serializable;
import java.util.UUID;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Generated
class RollEntityId implements Serializable {
  private UUID game;
  private int roll;

  public UUID getGame() {
    return game;
  }

  public void setGame(UUID game) {
    this.game = game;
  }

  public int getRoll() {
    return roll;
  }

  public void setRoll(int roll) {
    this.roll = roll;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(game).append(roll).hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }

    RollEntityId other = (RollEntityId) obj;
    return new EqualsBuilder().append(game, other.game).append(roll, other.roll).isEquals();
  }
}
