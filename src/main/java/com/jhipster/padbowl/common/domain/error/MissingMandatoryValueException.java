package com.jhipster.padbowl.common.domain.error;

public class MissingMandatoryValueException extends PadBowlException {

  protected MissingMandatoryValueException(PadBowlExceptionBuilder builder) {
    super(builder);
  }

  private static PadBowlExceptionBuilder builder(PadBowlMessage padBowlMessage, String fieldName, String message) {
    return PadBowlException.builder(padBowlMessage).status(ErrorStatus.INTERNAL_SERVER_ERROR).argument("field", fieldName).message(message);
  }

  private static String defaultMessage(String fieldName) {
    return "The field \"" + fieldName + "\" is mandatory and wasn't set";
  }

  public static MissingMandatoryValueException forNullValue(String fieldName) {
    return new MissingMandatoryValueException(
      builder(StandardMessage.SERVER_MANDATORY_NULL, fieldName, defaultMessage(fieldName) + " (null)")
    );
  }

  public static MissingMandatoryValueException forBlankValue(String fieldName) {
    return new MissingMandatoryValueException(
      builder(StandardMessage.SERVER_MANDATORY_BLANK, fieldName, defaultMessage(fieldName) + " (blank)")
    );
  }
}
