package com.jhipster.padbowl.common.domain;

import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.common.domain.error.MissingMandatoryValueException;
import com.jhipster.padbowl.common.domain.error.StringTooLongException;
import org.junit.jupiter.api.Test;

class UsernameUnitTest {

  @Test
  void shouldNotBuildWithoutUsername() {
    assertThatThrownBy(() -> new Username(null)).isExactlyInstanceOf(MissingMandatoryValueException.class).hasMessageContaining("username");
  }

  @Test
  void shouldNotBuildWithBlankUsername() {
    assertThatThrownBy(() -> new Username(" ")).isExactlyInstanceOf(MissingMandatoryValueException.class).hasMessageContaining("username");
  }

  @Test
  void shouldNotBuildWithTooLongUsername() {
    assertThatThrownBy(() -> new Username("d".repeat(51)))
      .isExactlyInstanceOf(StringTooLongException.class)
      .hasMessageContaining("username");
  }

  @Test
  void shouldGetUsername() {
    assertThat(user().get()).isEqualTo("user");
  }

  @Test
  void shouldNotBeEqualToNull() {
    assertThat(user().equals(null)).isFalse();
  }

  @Test
  @SuppressWarnings("unlikely-arg-type")
  void shouldNotBeEqualToAnotherType() {
    assertThat(user().equals("code")).isFalse();
  }

  @Test
  void shouldBeEqualToSelf() {
    Username username = user();

    assertThat(username.equals(username)).isTrue();
  }

  @Test
  public void shouldNotBeEqualToUsernameWithAnotherUsername() {
    assertThat(user().equals(new Username("toto"))).isFalse();
  }

  @Test
  public void shouldBeEqualToUsernameWithSameUsername() {
    assertThat(user().equals(user())).isTrue();
  }

  @Test
  public void shouldBeEqualToUsernameWithSameUsernameWithDifferentCase() {
    assertThat(user().equals(new Username("User"))).isTrue();
  }

  @Test
  public void shouldGetSameHashCodeWithSameUsername() {
    assertThat(user().hashCode()).isEqualTo(user().hashCode());
  }

  @Test
  public void shouldGetDifferentHashCodeWithDifferentUsername() {
    assertThat(user().hashCode()).isNotEqualTo(new Username("toto").hashCode());
  }

  private static Username user() {
    return new Username("user");
  }
}
