package com.jhipster.padbowl.game.domain;

import com.jhipster.padbowl.common.domain.error.ErrorStatus;
import com.jhipster.padbowl.common.domain.error.PadBowlException;

class InvalidRollException extends PadBowlException {

  public InvalidRollException() {
    super(PadBowlException.builder(GameMessage.INVALID_ROLL).message("A user tried to do an invalid roll").status(ErrorStatus.BAD_REQUEST));
  }
}
