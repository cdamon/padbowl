package com.jhipster.padbowl.common.domain;

import com.jhipster.padbowl.common.domain.error.Assert;
import com.jhipster.padbowl.game.domain.PlayerName;

public class Guttered {
  private final PlayerName playerName;

  public Guttered(PlayerName playerName) {
    Assert.notNull("playerName", playerName);

    this.playerName = playerName;
  }

  public PlayerName getPlayerName() {
    return playerName;
  }
}
