Feature: Playing Bowling games

  Scenario: Can't do invalid roll
    Given "Player1" creates a game
    When Player roll
      | 11 |
    Then The roll is invalid

  Scenario: Can't roll on somebody else's game
     Given "Player1" creates a game
     And I am logged in as "Player2"
     When Player roll
       | 0 |
     Then I should get an authorization error

  Scenario: Two rolls on a game
    Given "Player1" creates a game 
    When Player roll
      | 4 |
      | 3 |
    Then The score is 7
    And The game player name is "Player1"
    And The game frames are
      | First roll | Second roll |
      | 4          | 3           |
      
  Scenario: Shame gutter roll
    When Player roll
      | 0 |
    Then "Player1" is shamed