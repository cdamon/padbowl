package com.jhipster.padbowl.common.domain.error;

import java.io.Serializable;

@FunctionalInterface
public interface PadBowlMessage extends Serializable {
  String getMessageKey();
}
