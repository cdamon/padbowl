package com.jhipster.padbowl.common.domain.error;

public class StringTooLongException extends PadBowlException {

  private StringTooLongException(StringTooLongExceptionBuilder builder) {
    super(
      PadBowlException
        .builder(StandardMessage.STRING_TOO_LONG)
        .argument("field", builder.field)
        .argument("currentLength", builder.currentLength)
        .argument("maxLength", builder.maxLength)
        .message(builder.message())
        .status(ErrorStatus.INTERNAL_SERVER_ERROR)
    );
  }

  public static StringTooLongExceptionBuilder builder() {
    return new StringTooLongExceptionBuilder();
  }

  static class StringTooLongExceptionBuilder {
    private String field;
    private int currentLength;
    private int maxLength;

    public StringTooLongExceptionBuilder field(String field) {
      this.field = field;

      return this;
    }

    public StringTooLongExceptionBuilder currentLength(int currentLength) {
      this.currentLength = currentLength;

      return this;
    }

    public StringTooLongExceptionBuilder maxLength(int maxLength) {
      this.maxLength = maxLength;

      return this;
    }

    private String message() {
      return "Length of \"" + field + "\" must be under " + maxLength + " but was " + currentLength;
    }

    public StringTooLongException build() {
      return new StringTooLongException(this);
    }
  }
}
