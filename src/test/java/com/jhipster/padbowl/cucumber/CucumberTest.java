package com.jhipster.padbowl.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
  glue = "com.jhipster.padbowl",
  plugin = { "pretty", "json:target/cucumber/cucumber.json", "html:target/cucumber/cucumber.htm" },
  features = "src/test/features"
)
public class CucumberTest {}
