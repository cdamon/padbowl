package com.jhipster.padbowl.config;

import com.jhipster.padbowl.common.infrastructure.Generated;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Padbowl.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@Generated
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {}
